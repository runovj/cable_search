import numpy as np
import cv2
import math
import matplotlib.pyplot as plt

import seaborn as sns;

sns.set()  # for plot styling
from sklearn.cluster import KMeans
from random import randint


def draw_lines(img, lines, color=(255, 255, 255)):
    for line in lines:
        if len(line) == 4:
            x1, y1, x2, y2 = line
            img = cv2.line(img, (x1, y1), (x2, y2), color, 2)

    cv2.imshow('Found lines', img)


def draw_labeled_lines(img, lines, labels):
    n = max(labels) + 1
    colors = [(randint(0, 255), randint(0, 255), randint(0, 255)) for i in
              range(n)]

    for i in range(labels.shape[0]):
        class_color = colors[labels[i]]
        if len(lines[i]) == 4:
            x1, y1, x2, y2 = lines[i]
            img = cv2.line(img, (x1, y1), (x2, y2), class_color, 2)

    cv2.imshow('Found lines', lines_img)


def k_means(img, lines):
    n_clusters = min(lines.shape[0], 4)

    kmeans = KMeans(init='k-means++',
                    n_clusters=n_clusters, verbose=0)
    kmeans.fit(lines)
    print(f'Kmeans labels: {kmeans.labels_}')

    return kmeans.labels_



def my_hough_tf(img, lines):
    tf_lines = np.zeros((lines.shape[0], 2))

    for i in range(lines.shape[0]):
        x1, y1, x2, y2 = lines[i]

        if abs(x2 - x1) > 0.0001:
            angle = math.atan2(abs(y2 - y1), abs(x2 - x1))
            dist = abs(x1*(y2 - y1) + y1*(x1 - x2))/((x1 - x2)**2 + (y1 - y2)**2)**0.5

            tf_lines[i] = np.array([angle, dist])
            plt.scatter(angle, dist)
            plt.pause(0.005)

    labels = k_means(img, tf_lines)
    draw_labeled_lines(img, lines, labels)
    plt.draw()

    print(f'hough transform: {tf_lines}')

    cv2.waitKey()
    plt.clf()


if __name__ == '__main__':

    cap = cv2.VideoCapture('/home/ilya/Desktop/Samples/cable3_crop.mp4')

    while (True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        lines_img = np.zeros_like(frame)

        #         # Display the resulting frame
        #         cv2.imshow('frame', gray)

        # Create detector
        detector = cv2.ximgproc.createFastLineDetector(_length_threshold=25,
                                                       _distance_threshold=1.41421356,
                                                       _canny_th1=15.0,
                                                       _canny_th2=25.0,
                                                       _canny_aperture_size=3,
                                                       _do_merge=True)
        lines = None
        lines = detector.detect(gray, lines)

        try:
            # reshape (n, 1, 4) to (n, 4)
            lines = lines.reshape((lines.shape[0], 4))
            # draw_lines(lines_img, lines)
            my_hough_tf(lines_img, lines)
        except TypeError:
            pass
        except AttributeError:
            pass

        if cv2.waitKey(5) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()