#include "kmeans_line_filter.h"
#include <QDebug>

KMeans_line_filter::KMeans_line_filter()
{

}

void KMeans_line_filter::hough_transform(vector<cv::Vec4f> cartesian_lines,
                                         vector<cv::Point2f> &hough_lines)
{
    float x1, y1, x2, y2;
    for (auto itr : cartesian_lines) {
        x1 = itr[0];
        y1 = itr[1];
        x2 = itr[2];
        y2 = itr[3];

        if (abs(x2 - x1) > FLOAT_ACCURACY) {
            float angle = atan2(abs(y2 - y1), abs(x2 - x1));
            float dist = abs(x1*(y2 - y1) + y1*(x1 - x2)) /
                    sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));
            hough_lines.push_back(cv::Point2f(angle, dist));
        }
    }
}

bool KMeans_line_filter::fit(vector<cv::Point2f> lines,
                             cv::Mat &labels,
                             uint clusters_numb)
{
    // use K-means to label data
    cv::Mat centers;
    if (lines.size() >= clusters_numb) {
        cv::kmeans(lines, clusters_numb, labels, cv::TermCriteria(
                       cv::TermCriteria::EPS + cv::TermCriteria::COUNT,
                       10, 1.0),
                   10, cv::KMEANS_PP_CENTERS, centers);
        return true;
    }
    return false;
}

void KMeans_line_filter::filter(vector<cv::Vec4f> &lines,
                                vector<cv::Vec4f> &filtered_lines)
{
    std::vector<cv::Point2f> hough_lines;
    cv::Mat labels;
    hough_transform(lines, hough_lines);

    if (fit(hough_lines, labels)) {
        // split data into 2 clusters, keep data in hough transformation for kmeans
        vector<cv::Point2f> cluster0, cluster1;
        for (int i = 0; i < labels.rows; ++i) {
            if (labels.at<uchar>(cv::Point(0, i)) == 0) {
                cluster0.push_back(hough_lines.at(i));
            }
            else {
                cluster1.push_back(hough_lines.at(i));
            }
        }

        // calculate compactness of each cluster;
        cv::Mat labels0, labels1;
        double compactness0 = 0, compactness1 = 0;
        if (cluster0.size() > 0) {
            compactness0 = cv::kmeans(cluster0, 1, labels0,
                                   cv::TermCriteria(cv::TermCriteria::EPS +
                                                    cv::TermCriteria::COUNT,
                                                    10, 1.0),
                                   10, cv::KMEANS_PP_CENTERS);
        }
        if (cluster1.size() > 0) {
            compactness1 = cv::kmeans(cluster1, 1, labels1,
                                   cv::TermCriteria(cv::TermCriteria::EPS +
                                                    cv::TermCriteria::COUNT,
                                                    10, 1.0),
                                   10, cv::KMEANS_PP_CENTERS);
        }

        // return cluster with less compactness. TODO Why compactness doesn't work???
//        uint line_cluster_label = (compactness0 <= compactness1) ? 0 : 1;
        uint line_cluster_label = (cluster0.size() > cluster1.size()) ? 0 : 1;

        // fill output vector with lines from result cluster
        for (int i = 0; i < lines.size(); ++i) {
            if (labels.at<uchar>(cv::Point(0, i)) == line_cluster_label) {
                filtered_lines.push_back(lines.at(i));
            }
        }
    }

    else {
        filtered_lines = lines;
    }
}

void KMeans_line_filter::draw_lines(Mat &binary_img, vector<cv::Vec4f> &lines)
{
    for (auto itr : lines) {
        Point2f start, end;
        start.x = itr[0];
        start.y = itr[1];
        end.x = itr[2];
        end.y = itr[3];
        line(binary_img, start, end, 255, 3);
    }

    imshow("K-means result", binary_img);
}
