#ifndef MORPHOLOGYLINEFILTER_H
#define MORPHOLOGYLINEFILTER_H

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

class MorphologyLineFilter
{
public:
    MorphologyLineFilter();
    void filter(Mat &input, Mat &output);
    void erosion(Mat &input, Mat &output, int erosion_size,
                  int erosion_elem=2);
    void dilation(Mat &input, Mat &output, int dilation_size,
                  int dilation_elem=2);
};

#endif // MORPHOLOGYLINEFILTER_H
