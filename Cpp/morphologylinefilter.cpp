#include "morphologylinefilter.h"

MorphologyLineFilter::MorphologyLineFilter()
{

}

void MorphologyLineFilter::filter(Mat &input, Mat &output)
{
    dilation(input, output, 2);
    erosion(output, output, 2);
    erosion(output, output, 3);
}

/**  @function Erosion  */
void MorphologyLineFilter::erosion(Mat &input, Mat &output, int erosion_size,
                                   int erosion_elem)
{
    int erosion_type;
    if (erosion_elem == 0) erosion_type = MORPH_RECT;
    else if (erosion_elem == 1) erosion_type = MORPH_CROSS;
    else if (erosion_elem == 2) erosion_type = MORPH_ELLIPSE;

    Mat element = getStructuringElement(
                erosion_type, Size(2*erosion_size + 1, 2*erosion_size + 1),
                Point(erosion_size, erosion_size));

    /// Apply the erosion operation
    erode(input, output, element);
    imshow("Erosion Demo", output);
}

/** @function Dilation */
void MorphologyLineFilter::dilation(Mat &input, Mat &output, int dilation_size,
                                    int dilation_elem)
{
  int dilation_type;
  if (dilation_elem == 0) dilation_type = MORPH_RECT;
  else if (dilation_elem == 1) dilation_type = MORPH_CROSS;
  else if (dilation_elem == 2) dilation_type = MORPH_ELLIPSE;

  Mat element = getStructuringElement(
              dilation_type, Size(2*dilation_size + 1, 2*dilation_size + 1),
              Point(dilation_size, dilation_size));

  /// Apply the dilation operation
  dilate(input, output, element);
  imshow("Dilation", output);
}
