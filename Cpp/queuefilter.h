#ifndef QUEUEFILTER_H
#define QUEUEFILTER_H

#include <deque>
using namespace std;

class QueueFilter
{
public:
    QueueFilter(int queue_max_size = 15);
    void push(int lefty, int righty);
    void pop();
    void get_mean_val(int &lefty, int &righty);

private:
    int _max_size;
    deque<int> _q_lefty;
    deque<int> _q_righty;
};

#endif // QUEUEFILTER_H
