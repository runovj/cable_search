#include <QApplication>
#include <QWidget>
#include <QCoreApplication>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <opencv2/line_descriptor/descriptor.hpp>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/ximgproc/fast_line_detector.hpp>

#include <vector>
#include <deque>
#include <QDebug>
#include <iostream>

#include "lsdlinedetector.h"
#include "binarylinedetector.h"
#include "fastlinedetector.h"
#include "kmeans_line_filter.h"
#include "morphologylinefilter.h"
#include "queuefilter.h"

#define ACCURACY 0.0001
#define PIXEL_NUMB_THRESHOLD 200
<<<<<<< HEAD
#define QUEUE_FILTER_SIZE 10
=======
#define QUEUE_FILTER_SIZE 15
>>>>>>> bef9d2e93b570617e04b0eacc9621702383251b4

using namespace cv;
//using namespace std;

Mat img, gray_img, binary_img;
Mat dst, cdst, edges;

char* window_input_name = "Original";
char* contours_window_name = "Contours";

int ratio_threshold = 5;
int kernel_size = 3;
int contours_threshold_value = 100;
int area_threshold_value = 150;
RNG rng(12345);

void find_contours(const Mat &edges, Mat &drawing)
{
    /// Find contours
    std::vector<std::vector<cv::Point> > contours;
    std::vector<Vec4i> hierarchy;
    findContours( edges, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    qDebug() << "contours size = " << contours.size();

    /// Draw contours
    for (int i = 0; i< contours.size(); i++) {
        double p = arcLength(contours[i], true);
        double s = contourArea(contours[i]);
//        qDebug() << "perimeter = " << p;
//        qDebug() << "area = " << s;
        if (p < contours_threshold_value || s > area_threshold_value) {
            continue;
        }
//        Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255)); // color image
        Scalar color = Scalar(rng.uniform(0, 255)); // black_and_white image
        drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0) ;
    }

    /// Show in a window
    namedWindow( "Contours", CV_WINDOW_AUTOSIZE );
    imshow( "Contours", drawing);
};

void set_point_color(Mat &img, int col, int row, uchar val)
{
    img.at<uchar>(Point(col, row)) = val;
}

void queue_filter(int lefty_inp, int righty_inp,
                  int &lefty_m, int &righty_m)
{
<<<<<<< HEAD
    static deque<int> q_lefty, q_righty;
=======
    static deque<float> q_lefty, q_righty;
>>>>>>> bef9d2e93b570617e04b0eacc9621702383251b4
    if (q_lefty.empty()) {
        for (int i = 0; i < QUEUE_FILTER_SIZE; ++i) {
            q_lefty.push_back(lefty_m);
            q_righty.push_back(righty_m);
        }
    }
    else {
        q_lefty.push_back(lefty_inp);
        q_righty.push_back(righty_inp);
        q_lefty.pop_front();
        q_righty.pop_front();
    }

    lefty_m = righty_m = 0;
    for (auto itr : q_lefty) {
        lefty_m += itr;
    }
    for (auto itr : q_righty) {
        righty_m += itr;
    }
    lefty_m = static_cast<int>(lefty_m / QUEUE_FILTER_SIZE);
    righty_m = static_cast<int>(righty_m / QUEUE_FILTER_SIZE);
}

void main_line_detector()
{
    VideoCapture cap("/home/ilya/Desktop/Samples/cable3_crop.mp4");

    if (!cap.isOpened()) {
        return;
    }

//    LSDLineDetector ld;
//    BinaryLineDetector ld;
    FastLineDetector ld;
    KMeans_line_filter k_means_filter;
    MorphologyLineFilter morphology_filter;
    Mat binary_img = Mat::zeros(img.rows, img.cols, CV_8UC1);
    QueueFilter q_filter(QUEUE_FILTER_SIZE);

    while (cap.read(img)) {
        imshow("Input", img);
//        morphology_filter.filter(img, img);
        cvtColor(img, gray_img, CV_BGR2GRAY);
        vector<Vec4f> lines = ld.detect(gray_img);
        vector<Vec4f> filtered_lines;
        k_means_filter.filter(lines, filtered_lines);

        Mat res, binary_img = Mat::zeros(img.rows, img.cols, CV_8UC1);
        binary_img.copyTo(res);
        k_means_filter.draw_lines(binary_img, filtered_lines);

        morphology_filter.filter(binary_img, binary_img);

        vector<Point> white_pixels;
        findNonZero(binary_img, white_pixels);
        if (white_pixels.size() > PIXEL_NUMB_THRESHOLD) {
            Vec4f line;
            fitLine(white_pixels, line, DIST_L2, 0, 0.01, 0.01);
            float vx, vy, x, y;
            vx = line[0];
            vy = line[1];
            x = line[2];
            y = line[3];

            int lefty, righty, lefty_m, righty_m;
            lefty = int((-x*vy/vx) + y);
            righty = int(((img.cols - x)*vy/vx) + y);
<<<<<<< HEAD
            q_filter.push(lefty, righty);

        }
        int lefty_m, righty_m;
        q_filter.get_mean_val(lefty_m, righty_m);
        q_filter.pop();
        if (lefty_m != 0 and righty_m != 0) {
=======
//           lefty_m, righty_m = low_freq_filter(lefty, righty)
            queue_filter(lefty, righty, lefty_m, righty_m);
>>>>>>> bef9d2e93b570617e04b0eacc9621702383251b4
            cv::line(res, Point(img.cols - 1, righty_m), Point(0, lefty_m), 255, 2);
        }

        imshow("Result", res);

        if (static_cast<int>(waitKey(5)) == 'q') {
            break;
        }
    }
}

int main(int argc, char** argv)
{

//    QCoreApplication a(argc, argv);

    main_line_detector();
}

