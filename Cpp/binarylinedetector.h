#ifndef BINARYLINEDETECTOR_H
#define BINARYLINEDETECTOR_H

#include <opencv2/line_descriptor.hpp>

#include "opencv2/core/utility.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/utils/trace.hpp"
#include "opencv2/core_detect.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QDebug>

class BinaryLineDetector
{
public:
    BinaryLineDetector();

    void detect(const cv::Mat &img);
    void show(cv::Mat &output);
private:
    cv::Ptr<cv::line_descriptor::BinaryDescriptor> _detector_ptr;
    std::vector<cv::line_descriptor::KeyLine> _lines;
};

#endif // BINARYLINEDETECTOR_H
