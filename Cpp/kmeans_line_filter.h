#ifndef KMEANS_LINE_FILTER_H
#define KMEANS_LINE_FILTER_H

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <opencv2/line_descriptor/descriptor.hpp>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/ximgproc/fast_line_detector.hpp>

#define FLOAT_ACCURACY  0.0001

using namespace std;
using namespace cv;

class KMeans_line_filter
{
public:
    KMeans_line_filter();
    void filter(vector<cv::Vec4f> &lines,
                vector<cv::Vec4f> &filtered_lines);
    void draw_lines(Mat &binary_img, vector<cv::Vec4f> &lines);

private:
    void hough_transform(vector<cv::Vec4f> cartesian_lines,
                         vector<cv::Point2f> &hough_lines);
    bool fit(vector<cv::Point2f> lines, cv::Mat &labels,
             uint clusters_numb=2);
};

#endif // KMEANS_LINE_FILTER_H
