#include "lsdlinedetector.h"

LSDLineDetector::LSDLineDetector() :
    _detector_ptr(new cv::line_descriptor::LSDDetector)
{
}

LSDLineDetector::~LSDLineDetector()
{
    delete _detector_ptr;
}

void LSDLineDetector::detect(const cv::Mat &img, int scale, int octaves_numb)
{
    if (_lines.size() != 0) {
        _lines.clear();
    }
    _detector_ptr->detect(img, _lines, scale, octaves_numb);
}

void LSDLineDetector::show(cv::Mat &output)
{
    for (size_t i = 0; i < _lines.size(); i++) {
        cv::line_descriptor::KeyLine kl = _lines[i];

        if (kl.octave == 0) {
          /* get a random color */
          int R = (rand() % (int)(255 + 1));
          int G = (rand() % (int)(255 + 1));
          int B = (rand() % (int)(255 + 1));

          /* get extremes of line */
          cv::Point pt1 = cv::Point(kl.startPointX, kl.startPointY);
          cv::Point pt2 = cv::Point(kl.endPointX, kl.endPointY);

          /* draw line */
          cv::line(output, pt1, pt2, cv::Scalar( B, G, R ), 5);
        }
    }

    /* show lines on image */
    cv::imshow("Lines", output );
}
