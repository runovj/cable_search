#include "queuefilter.h"

QueueFilter::QueueFilter(int queue_max_size)
{
    _max_size = queue_max_size;
}

void QueueFilter::push(int lefty, int righty)
{
    if (!_q_lefty.empty() or !_q_righty.empty()) {
        _q_lefty.push_back(lefty);
        _q_righty.push_back(righty);
    }
    else {
        for (int i = 0; i < _max_size; ++i) {
            _q_lefty.push_back(lefty);
            _q_righty.push_back(righty);
        }
    }
}

void QueueFilter::pop()
{
    if (!_q_lefty.empty() or !_q_righty.empty()) {
        _q_lefty.pop_front();
        _q_righty.pop_front();
    }
}

void QueueFilter::get_mean_val(int &lefty, int &righty)
{
    lefty = righty = 0;
    if (!_q_lefty.empty() or !_q_righty.empty()) {

        for (auto itr : _q_lefty) {
            lefty += itr;
        }

        for (auto itr : _q_righty) {
            righty += itr;
        }

        lefty = static_cast<int>(lefty / _q_lefty.size());
        righty = static_cast<int>(righty / _q_righty.size());
    }
}
