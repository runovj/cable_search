#include "fastlinedetector.h"

/// Global variables
char* window_name = "Morphology Transformations Demo";
Mat inp, res;

FastLineDetector::FastLineDetector(int length_threshold,
                                   float distance_threshold,
                                   double canny_th1, double canny_th2,
                                   int canny_aperture_size, bool do_merge)
{
    _detector = cv::ximgproc::createFastLineDetector(length_threshold,
                                       distance_threshold,
                                       canny_th1, canny_th2,
                                       canny_aperture_size, do_merge);

}

vector<Vec4f> FastLineDetector::detect(const Mat &img)
{
    _lines.clear();
    _detector->detect(img, _lines);
    return _lines;
}

void FastLineDetector::show(const Mat &img)
{
    Mat line_image_fld(img);
    _detector->drawSegments(line_image_fld, _lines);
    imshow("FLD result", line_image_fld);
}

void FastLineDetector::show_binary(const Mat &img)
{
    Mat binary_img = Mat::zeros(img.rows, img.cols, CV_8UC1);
    Mat hough = Mat::zeros(img.rows, img.cols, CV_8UC1);
//    _detector->drawSegments(black_img, _lines, true);

    for (auto itr : _lines) {
        Point2f start, end;
        start.x = itr[0];
        start.y = itr[1];
        end.x = itr[2];
        end.y = itr[3];
        line(binary_img, start, end, 255, 3);
    }

    imshow("FLD result", binary_img);
    binary_img.copyTo(inp);
}

