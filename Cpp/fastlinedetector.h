#ifndef FASTLINEDETECTOR_H
#define FASTLINEDETECTOR_H

#include <iostream>
#include "opencv2/imgproc.hpp"
#include "opencv2/ximgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include <QDebug>

using namespace std;
using namespace cv;
//using namespace cv::ximgproc;


class FastLineDetector
{
public:
    FastLineDetector(int length_threshold = 35,
                     float distance_threshold = 1.41421356f,
                     double canny_th1 = 25.0,
                     double canny_th2 = 35.0, int canny_aperture_size = 3,
                     bool do_merge = true);

    vector<Vec4f> detect(const Mat &img);
    void show(const Mat &img);

    void show_binary(const Mat &img);

private:
    Ptr<cv::ximgproc::FastLineDetector> _detector;
    vector<Vec4f> _lines;
};

#endif // FASTLINEDETECTOR_H
