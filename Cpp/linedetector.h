#ifndef LINEDETECTOR_H
#define LINEDETECTOR_H

#include <opencv2/line_descriptor.hpp>

#include "opencv2/core/utility.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/utils/trace.hpp"
#include "opencv2/core_detect.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QDebug>
#include <iostream>

class LineDetector
{
public:
    LineDetector();
    ~LineDetector();

    void detect(const cv::Mat &img, int scale, int octaves_numb);
    void show(cv::Mat &output);
private:
    cv::line_descriptor::LSDDetector *_detector_ptr;
    std::vector<cv::line_descriptor::KeyLine> _lines;

};

#endif // LINEDETECTOR_H
